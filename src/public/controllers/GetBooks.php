<?php


class GetBooks {
    private $db_host;
    private $db_user;
    private $db_pass;
    private $db_database;
    private $link;



    public function __construct(){
        include 'utils/Config.php';
        $this->db_host = $config['db_host'];
        $this->db_user = $config['db_user'];
        $this->db_pass = $config['db_pass'];
        $this->db_database = $config['db_database'];

        $this->link = mysqli_connect($this->db_host, $this->db_user, $this->db_pass, $this->db_database);
        mysqli_set_charset($this->link, "utf8");
        if (!$this->link) {
            echo "Error: Unable to connect to MySQL." . PHP_EOL;
            echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
            echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
            exit;
        }

    }
  
    public function search( $search , $page , $items_per_page ){ 

        $sql = 'SELECT COUNT(*) FROM old_books '. $this->createSearchQuery($search) . ';';
        $result = $this->link->query($sql);
        $pages = ($result->fetch_assoc()['COUNT(*)']) / $items_per_page;

        $sql = 'SELECT * FROM old_books '. $this->createSearchQuery($search) .' LIMIT ' . $items_per_page * ($page-1).','.$items_per_page.' ' ;
        $result = $this->link->query($sql);
        if ($result->num_rows > 0){
            echo "<table><tr><th>Code</th><th>Title/Τίτλος</th><th>Author/Συγγραφέας</th></tr>";
            while($row = $result->fetch_assoc()){
                echo '<tr><td>' . $row['code'] . '</td><td>'. $row['title'] . '</td>';
                echo '<td>';
                if ($row['author1']){
                    echo $row['author1'];
                }
                if ($row['author2']){
                    echo ', '.$row['author2'];
                }
                if ($row['author3']){
                    echo ', '.$row['author3'];
                }
                echo '</td></tr>';
            }
            echo '</table>';
        }else{
            echo 'No Results!';
        }

        echo $this->paginate( $page , $pages , $search );

        $this->close();
        
        
    }

    public function get_all( $page , $items_per_page ){ 

        $sql = 'SELECT COUNT(*) FROM old_books '. $this->createSearchQuery(null);
        $result = $this->link->query($sql);
        $pages = ($result->fetch_assoc()['COUNT(*)']) / $items_per_page;

        $sql = 'SELECT * FROM old_books ' . $this->createSearchQuery(null) . ' LIMIT '. $items_per_page * ($page-1).', '.$items_per_page.';';
        $result = $this->link->query($sql);
        if ($result->num_rows > 0){
            echo "<table><tr><th>Code</th><th>Title/Τίτλος</th><th>Author/Συγγραφέας</th></tr>";
            while($row = $result->fetch_assoc()){
                echo '<tr><td>' . $row['code'] . '</td><td>'. $row['title'] . '</td>';
                echo '<td>';
                if ($row['author1']){
                    echo $row['author1'];
                }
                if ($row['author2']){
                    echo ', '.$row['author2'];
                }
                if ($row['author3']){
                    echo ', '.$row['author3'];
                }
                echo '</td></tr>';
            }
            echo '</table>';
        }else{
            echo 'No Results!';
        }

        echo $this->paginate( $page , $pages , null );

        $this->close();
    }

    private function paginate( $current_page , $pages, $search ){
        
        echo '<form name="pagination_form" action="" method="GET">';
        echo '<br><select name="page" onchange="this.form.submit();">';
        for ($i = 1; $i <= $pages; $i++){
            echo '<option';
            if($current_page==$i){echo ' selected ';}
            echo '>'.$i.'</option>';
        }

        echo '<br>Current Page: '.$current_page.'/'.$pages.'<br>';
        if($search!=null){
            echo '<input type="hidden" name="string_search" value="'.$search.'">';
        }
        echo '</form>';
    }

    private function close(){
        mysqli_close($this->link);
    }

    private function createSearchQuery($search){
        $search =  mysqli_real_escape_string($this->link, $search);
        $q = 'WHERE is_hidden = 0 ';
        if ($search!=null){
            $q .= ' AND ( code LIKE "%'. $search .'%" OR title LIKE "%' . $search . '%" OR  author1 LIKE "%' . $search . '%"  OR  author2 LIKE "%' . $search . '%"  OR  author3 LIKE "%' . $search . '%") '; 
        }
        return $q;
    }

}



/*
phpinfo();
*/

?>
