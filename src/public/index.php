<?php

if (  getenv('PHP_DEBUGGING') ){
  //error_reporting(-1);
  error_reporting(E_ERROR | E_PARSE);
  ini_set('display_errors', 'On');
}

  include 'utils/Redirect.php';
  include 'utils/Config.php';
  
  if($config['site_maintenance_mode']){
    require __DIR__ . $config['maintenance_url'];
    exit();
  }


  session_start(); // Start Session for all pages

  require 'utils/AltoRouter.php';
  require 'utils/Translate.php';

  // Initialize Router
  $router = new AltoRouter();

  // Select Timezone
  date_default_timezone_set($config['timezone']);


  // Create Paths

  $router->map( 'GET', '/', function() {  require __DIR__ . '/views/Home.php'; }, 'Home_GET');
  $router->map( 'POST', '/', function() {  require __DIR__ . '/views/Home.php'; }, 'Home_POST');
  
 
  // Old Books
  $router->map( 'GET', '/old_books', function() { require __DIR__ . '/views/Old_Books.php'; }, 'Old_Books_GET');
  $router->map( 'POST', '/old_books', function() { require __DIR__ . '/views/Old_Books.php'; }, 'Old_Books_POST');


  // Match Routes and Load file
  $match = $router->match();
  if( $match && is_callable( $match['target'] ) ) {
	   call_user_func_array( $match['target'], $match['params'] );
  } else {
	   // no route was matched
	   //header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
     require __DIR__ . '/views/error-404.php';
  }


?>
