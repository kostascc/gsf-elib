<?php require_once 'public_header.php'; ?>

<div class="row">
  <div class="col-12">
    <div class="card">

      <a href="/old_books"><h2>Greek School of Frankfurt ~ e-Lib</h2></a>
      <a href="/old_books">Enter / Είσοδος</a>

      <br><br>
      <p>Μέσω μίας ερευνητικής εργασίας κατα την περίοδο 2014-2015, μέρος της βιλβιοθήκης του Ελληνικού Σχολείου Φρανκφούρτης καταγράφηκε ηλεκτρονικά και ταξινομήθηκε. Το αρχείο με στοιχεία για περίπου 1140 βιβλία σώζεται στην εφαρμογή <a href="/old_books">GSF e-Lib</a>.</p> 
    
    </div>
  </div>
</div>

<?php require_once 'footer.php'; ?>
