<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'utils/Config.php'; ?>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon.ico">
    <title><?php echo $config['header_title']; ?></title>
    
    <link href="/assets/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/helper.css" rel="stylesheet">

    <link href="/assets/css/style.css" rel="stylesheet">
</head>

<body class="fix-header fix-sidebar">

    <div id="main-wrapper">
        
            <div class="container-fluid">
