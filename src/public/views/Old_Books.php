<?php require_once 'public_header.php'; ?>

<a href="/">Exit / Έξοδος</a>
<div class="row">
  <div class="col-12">
    <div class="card">


      <form id="clear_form" method="GET" action=""></form>

      <form id="search_form" method="GET" action="">
          
        <div class="row">

          <!-- Left Col -->
          <div class="col-md-6">
              <input class="form-control form-control-line" placeholder="Search" type="text" name="string_search"
                <?php if(isset($_GET['string_search'])){ echo 'value="'.$_GET['string_search'].'"' ;} ?>>
          </div>

          <!-- Right Col --
          <div class="col-md-4">
            <select name="search_type" class="form-control form-control-line">
            <option <?php if(isset($_GET['search_type'])&&($_GET['search_type']=='Title')){echo ' selected ';}?>>Title</option>
            <option <?php if(isset($_GET['search_type'])&&($_GET['search_type']=='Code')){echo ' selected ';}?>>Code</option>
            <option <?php if(isset($_GET['search_type'])&&($_GET['search_type']=='Author')){echo ' selected ';}?>>Author</option>
            </select>
          </div>
          -->

        </div>
 
        <br>

        <div class="row">
        
          <div class="col-md-12">
            <button class="btn btn-warning" action="submit" form="clear_form">Clear/Καθαρισμός</button>
            <button class="btn btn-success" action="submit" form="search_form">Search/Αναζήτηση</button>
          </div>
        </div>

      </form>


    </div>
  </div>
</div>


<div class="row">
  <div class="col-12">
    <div class="card">

      <?php

        require_once 'controllers/GetBooks.php';

        $items_per_page = 20;
        $page = 1;
        $string_search = '';


        if(isset($_GET['page'])){
          $page = (int)$_GET['page'];
        }
        if(isset($_GET['string_search'])){
          $string_search = $_GET['string_search'];
        }

        $getBooks = new GetBooks();

        if(!isset($_GET['string_search'])){
          $getBooks->get_all( $page , $items_per_page );
        }else{
          $getBooks->search( $string_search, $page , $items_per_page );
        }

      ?>

    </div>
  </div>
</div>

<?php require_once 'footer.php'; ?>
