build:
	docker build --no-cache -t nginx .
 
run:
	docker run -d -p 4001:80 --name fralib ubuntu

clean:
	docker stop fralib && docker rm fralib -v