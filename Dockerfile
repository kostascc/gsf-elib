FROM registry.gitlab.com/pi-squared/simple-apache-php-container

COPY ./src/public /var/www/html

ENV TIMEZONE=Europe/Athens      \
    HEADER_TITLE="GSF e-Lib"    \
    PHP_DEBUGGING=0             \
    FOOTER_TITLE="GSF e-Lib"    \
    ANALYTICS_ID=""